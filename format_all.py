import sys
import os

def format_file(file):
  print file

  command = "clang-format " + file
  output = os.popen(command).read()

  f = open(file,'w')
  f.write(output)
  f.close()

# -------------- main --------------
if __name__ == '__main__':

  directory = os.path.dirname(os.path.abspath(__file__))

  for root, dirs, files in os.walk(directory):
    for file in files:
      if file.endswith(".cpp"):
        format_file(os.path.join(root, file))
      elif file.endswith(".h"):
        if file != "Shaders.h":
          format_file(os.path.join(root, file))
